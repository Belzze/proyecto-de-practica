public with sharing class extEditarContactos {
    public String idUsuario{get;set;}
    
    private ApexPages.StandardController stdController;
    ContactoPortal__c contactop;
    
    public extEditarContactos( ApexPages.StandardController ct) {
        idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
        this.stdController = ct;
        if( ! Test.isRunningTest()){
            // You can't call addFields() in a test context, it's a bug
            stdController.addFields(accessibleFields);
        }
        contactop = (ContactoPortal__c) ct.getRecord();
    }

    public List<string> accessibleFields{
        get{
                
            if (accessibleFields == null){
                // Get a list (map) of all fields on the object
                Map<String,Schema.SObjectField> fields =
                    Schema.SObjectType.ContactoPortal__c.fields.getMap();

                // Save only the fields accessible by the current user
                Set<String> availableFieldsSet = new Set<String>();
                for (String s : fields.keySet()){
                    if (fields.get(s).getDescribe().isAccessible()
                        // Comment out next line to show standard/system fields
                        && fields.get(s).getDescribe().isCustom()
                        
                    ){
                        if(s.containsIgnoreCase('Usuario_Portal__c')){
                            
                        }else{
                            availableFieldsSet.add(s.toLowerCase());
                        }
                            
                        
                        if(Test.isRunningTest()) System.debug('Field: ' + s);
                    }
                }

                //Conver set to list, save to property        
                accessibleFields = new List<String>(availableFieldsSet);
                accessibleFields.add(Usuario_Portal__c='a00f4000007mqMSAAY');
            
            }
            return accessibleFields;
        }
        private set;
    }
    public PageReference cancelarr(){
        PageReference can = new PageReference('/apex/vfContactos?idUsuario='+contactop.Usuario_Portal__c);
        can.setRedirect(true);
        return can;
        
    }
    
    public PageReference guardar(){
                update contactop;
            PageReference pr = new PageReference('/apex/vfContactos?idUsuario='+contactop.Usuario_Portal__c);
        	pr.setRedirect(true);
            return pr; 
    }
    public PageReference guardar2(){
        insert contactop;
		PageReference pr = new PageReference('/apex/vfContactos?idUsuario='+idUsuario);  
        pr.setRedirect(true);
        return pr;
    }
}