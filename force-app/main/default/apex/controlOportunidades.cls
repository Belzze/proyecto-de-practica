public with sharing class controlOportunidades {
    public string idUsuario{get;set;}
    Usuario_Portal__c usu;
    public controlOportunidades() {
        try{
            idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
            if(idUsuario != null && idUsuario != ''){
                usu = [Select Id, Name 
                        From Usuario_Portal__c Where id =: idUsuario];
            }
        }catch(Exception e){
                        Apexpages.addMessage(new Apexpages.Message( Apexpages.Severity.ERROR, 'Usuario no encontrado'));
        }
    }
    public ApexPages.StandardSetController setCon{
        get {
            if(setCon == null){
                setCon = new Apexpages.StandardSetController(Database.getQueryLocator(
                    [SELECT Cliente__c,Fecha_de_cierre__c,Importe__c,Name,Tipo__c,Usuario_Portal__c FROM OportunidadPortal__c  WHERE Usuario_Portal__c =: usu.Id ORDER BY CreatedDate DESC]
                ));
            }
            return setCon;
        }
        set;
    }

    public List<OportunidadPortal__c> getOportunidades() {
        return (List<OportunidadPortal__c>) setCon.getRecords();
    }    
}