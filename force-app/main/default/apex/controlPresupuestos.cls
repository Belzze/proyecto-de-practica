public with sharing class controlPresupuestos {
    public String idUsuario{get;set;}
    Usuario_Portal__c usu;
    public controlPresupuestos() {
        try{
            idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
            if(idUsuario != null && idUsuario != ''){
                usu = [Select Id, Name 
                        From Usuario_Portal__c Where id =: idUsuario];
            }
        }catch(Exception e){
            Apexpages.addMessage(new Apexpages.Message( Apexpages.Severity.ERROR, 'Usuario no encontrado'));
        }
    }
    public ApexPages.StandardSetController setCon{
        get {
            if(setCon == null){
                setCon = new Apexpages.StandardSetController(Database.getQueryLocator(
                    [SELECT ExpirationDate,IsSyncing,Name,OpportunityId,Subtotal,TotalPrice FROM Quote  WHERE Usuario_Portal__c =: usu.Id ORDER BY CreatedDate DESC]
                ));
            }
            return setCon;
        }
        set;
    }

    public List<Quote> getPresupuestos() {
        return (List<Quote>) setCon.getRecords();
    } 
}