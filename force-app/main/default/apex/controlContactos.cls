public with sharing class controlContactos {
    public string idUsuario{get;set;}
    Usuario_Portal__c usu;
    public controlContactos() {
        try
        {
            idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
            if(idUsuario != null && idUsuario != ''){
                usu = [Select Id, Name 
                        From Usuario_Portal__c Where id =: idUsuario];
            }
        }catch(Exception e){
            Apexpages.addMessage(new Apexpages.Message( Apexpages.Severity.ERROR, 'Usuario no encontrado'));
        }
    }

    public ApexPages.StandardSetController setCon{
        get {
            if(setCon == null){
                setCon = new Apexpages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Cliente_Portal__r.Name,Correo__c,Name,Apellidos__c,Phone__c FROM ContactoPortal__c WHERE Usuario_Portal__c =: usu.Id ORDER BY CreatedDate DESC]));
            }
            return setCon;
        }
        set;
    }
	
    public List<ContactoPortal__c> getContacts() {
        return (List<ContactoPortal__c>) setCon.getRecords();
    }
}