public with sharing class extEditarOportunidades {

    private ApexPages.StandardController stdController;
    OportunidadPortal__c oportunip;

    public extEditarOportunidades(ApexPages.StandardController ct) {
        this.stdController = ct;
        if( ! Test.isRunningTest()){
            stdController.addFields(accessibleFields);
        }
        oportunip = (OportunidadPortal__c) ct.getRecord();
    }

    public List<String> accessibleFields{
        get{
            if (accessibleFields == null){
                // Get a list (map) of all fields on the object
                Map<String,Schema.SObjectField> fields = Schema.SObjectType.OportunidadPortal__c.fields.getMap();

                // Save only the fields accessible by the current user
                Set<String> availableFieldsSet = new Set<String>();
                for (String s : fields.keySet()){
                    if (fields.get(s).getDescribe().isAccessible()
                        // Comment out next line to show standard/system fields
                        && fields.get(s).getDescribe().isCustom()
                        
                    ){
                        availableFieldsSet.add(s.toLowerCase());
                        if(Test.isRunningTest()) System.debug('Field: ' + s);
                    }
                }

                //Conver set to list, save to property
                accessibleFields = new List<String>(availableFieldsSet);
            
            }
            return accessibleFields;
        }
        private set;
    }
    public PageReference cancelarr(){
        PageReference can = new PageReference('/apex/vfOportunidades?idUsuario='+oportunip.Usuario_Portal__c);
        can.setRedirect(true);
        return can;
        
    }
    
    public PageReference guardar(){
                update oportunip;
            PageReference pr = new PageReference('/apex/vfOportunidades?idUsuario='+oportunip.Usuario_Portal__c);
        	pr.setRedirect(true);
            return pr; 
    }
}
