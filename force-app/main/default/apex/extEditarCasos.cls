public with sharing class extEditarCasos {

    private ApexPages.StandardController stdController;
    Casos_Portal__c casop;

    public extEditarCasos(ApexPages.StandardController ct) {
        this.stdController = ct;
        if( ! Test.isRunningTest()){
            stdController.addFields(accessibleFields);
        }
        casop = (Casos_Portal__c) ct.getRecord();
    }

    public List<String> accessibleFields{
        get{
            if (accessibleFields == null){
                // Get a list (map) of all fields on the object
                Map<String,Schema.SObjectField> fields =
                    Schema.SObjectType.Casos_Portal__c.fields.getMap();

                // Save only the fields accessible by the current user
                Set<String> availableFieldsSet = new Set<String>();
                for (String s : fields.keySet()){
                    if (fields.get(s).getDescribe().isAccessible()
                        // Comment out next line to show standard/system fields
                        && fields.get(s).getDescribe().isCustom()
                    ){
                        availableFieldsSet.add(s.toLowerCase());
                        if(Test.isRunningTest()) System.debug('Field: ' + s);
                    }
                }

                //Conver set to list, save to property
                accessibleFields = new List<String>(availableFieldsSet);
            
            }
            return accessibleFields;
        }
        private set;
    }
    public PageReference cancelarr(){
        PageReference can = new PageReference('/apex/vfCasos?idUsuario='+casop.Usuario_Portal__c);
        can.setRedirect(true);
        return can;
        
    }
    
    public PageReference guardar(){
                update casop;
            PageReference pr = new PageReference('/apex/vfCasos?idUsuario='+casop.Usuario_Portal__c);
        	pr.setRedirect(true);
            return pr; 
    }
}
