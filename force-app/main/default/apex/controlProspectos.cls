public with sharing class controlProspectos {
    public String idUsuario{get;set;}
    Usuario_Portal__c usu;
    public controlProspectos() {
        try{
            idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
            if(idUsuario != null && idUsuario != ''){
                usu = [Select Id, Name
                        FROM Usuario_Portal__c Where id =: idUsuario];
            }
        }catch(Exception e){
            Apexpages.addMessage(new ApexPages.Message( Apexpages.Severity.ERROR, 'Usuario no encontrado'));
        }
    }

    public Apexpages.StandardSetController setCon{
        get{
            if(setCon == null){
                setCon = new Apexpages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name,Title,Company,Phone,Email,Status FROM Lead WHERE Usuario_Portal__c =: usu.Id ORDER BY CreatedDate DESC]                    
                ));
            }
            return setCon;
        }
        set;
    }
    
    public List<Lead> getProspects() {
        return (List<Lead>) setCon.getRecords();
    }
}