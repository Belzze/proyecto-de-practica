public with sharing class extCrearPresupuestos {
    public String idUsuario{get;set;}

    private ApexPages.StandardController stdController;
    Quote oportunip;

    public extCrearPresupuestos(ApexPages.StandardController controller) {
        idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
        controller.addFields(editableFields);
        oportunip = (Quote) controller.getRecord();
        oportunip.Usuario_Portal__c = idUsuario;
    }

    public List<String> editableFields{
        get{
            if (editableFields == null){
                // Get a list (map) of all fields on the object
                Map<String,Schema.SObjectField> fields =
                    Schema.SObjectType.Quote.fields.getMap();
                
                // Save only the fields accessible by the current user
                Set<String> availableFieldsSet = new Set<String>();
                for (String s : fields.keySet()){
                    if (fields.get(s).getDescribe().isAccessible() 
                        // Comment out next line to show standard/system fields
                        && fields.get(s).getDescribe().isCustom()
                    ){
                        if(s.containsIgnoreCase('Usuario_Portal__c')){
                            
                        }else{
                            availableFieldsSet.add(s.toLowerCase());
                        }
                            
                        
                        if(Test.isRunningTest()) System.debug('Field: ' + s);
                    }
                }

                //Conver set to list, save to property
                editableFields = new List<String>(availableFieldsSet);
            
            }
            return editableFields;
        }
        private set;
    }

    public PageReference guardar2(){
        insert oportunip;
		PageReference pr = new PageReference('/apex/vfPresupuestos?idUsuario='+idUsuario);  
        pr.setRedirect(true);
        return pr;
    }
}