public with sharing class controlCasos {
    public string idUsuario{get;set;}
    Usuario_Portal__c usu;
    public controlCasos() {
        try{
            idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
            if(idUsuario != null && idUsuario != ''){
                usu = [Select Id, Name 
                        From Usuario_Portal__c Where id =: idUsuario];
            }
        }catch(Exception e){
                        Apexpages.addMessage(new Apexpages.Message( Apexpages.Severity.ERROR, 'Usuario no encontrado'));
        }
    }
    public ApexPages.StandardSetController setCon{
        get {
            if(setCon == null){
                setCon = new Apexpages.StandardSetController(Database.getQueryLocator(
                    [SELECT CreatedDate,Estado__c,Id,Name,Nombre_cliente__c,Nombre_contacto__c,Numero_de_caso__c,Prioridad__c,Usuario_Portal__c FROM Casos_Portal__c WHERE Usuario_Portal__c =: usu.Id ORDER BY CreatedDate DESC]));
            }
            return setCon;
        }
        set;        
    }

    public List<Casos_Portal__c> getCasos() {
        return (List<Casos_Portal__c>) setCon.getRecords();
    }
}
