public with sharing class extEditarProspectos {
    public String idUsuario{get;set;}
    
    private ApexPages.StandardController stdController;
    Lead prospectop;
    
    public extEditarProspectos( ApexPages.StandardController controller) {
        idUsuario = Apexpages.currentPage().getParameters().get('idUsuario');
        controller.addFields(editableFields); 
        prospectop = (Lead) controller.getRecord();
        prospectop.Usuario_Portal__c = idUsuario;
    }

    public List<string> editableFields{
        get{
            if (editableFields == null) {
                Map<String, Schema.SobjectField> fields = 
                    Schema.SobjectType.Lead.fields.getMap();
                Set<String> availableFieldsSet = new Set<String>();
                for (String s : fields.keySet()) {
                    if (fields.get(s).getDescribe().isAccessible() 
                        // Comment out next line to show standard/system fields
                        && fields.get(s).getDescribe().isCustom()
                    ){
                        if(s.containsIgnoreCase('Usuario_Portal__c')){
                            
                        }else{
                            availableFieldsSet.add(s.toLowerCase());
                        }
                            
                        
                        if(Test.isRunningTest()) System.debug('Field: ' + s);
                    }
                }
                
				editableFields = new List<String>(availableFieldsSet);                
                
            }
            return editableFields ;
        }
        private set;
    }
    public PageReference cancelarr(){
        PageReference can = new PageReference('/apex/vfProspectos?idUsuario='+idUsuario);
        can.setRedirect(true);
        return can;
        
    }
    
    public PageReference guardar(){
                update prospectop;
            PageReference pr = new PageReference('/apex/vfProspectos?idUsuario='+idUsuario);
        	pr.setRedirect(true);
            return pr; 
    }
    public PageReference guardar2(){
        insert prospectop;
		PageReference pr = new PageReference('/apex/vfProspectos?idUsuario='+idUsuario);  
        pr.setRedirect(true);
        return pr;
    }
}